#include <stdio.h>
#include <stdlib.h>

void main() {
    int tam, valor = 0;
    printf("Ingrese el tamaño del vector\n");
    scanf("%d", &tam);
        int vec[tam], i, a;
        for (i = 0; i < tam; i++) {
            printf("Ingrese un valor en la posición %d\n", i+1);
            scanf("%d", &vec[i]);
            for (a = 0; a < i; a++) {
				//validamos que no sea repetido
                while (vec[a] == vec[i]) {
                    printf("Valor ya registrado, Ingrese un valor en la posición %d\n", i + 1);
                    scanf("%d", &vec[i]);
                }
            }
        }

		//imprimimos el vector desordenado
        printf("\nVector desordenado:\n ");
        for (i = 0; i < tam; i++) {
            printf("%d  ", vec[i]);
        }
		
		//ordenamiento
        printf("\n");
        int mayorN;
        for (i = 0; i < tam; i++) {
            for (a = 0; a < tam; a++) {
                if (vec[a] < vec[i]) {
                    mayorN = vec[i];
                    vec[i] = vec[a];
                    vec[a] = mayorN;
                }
            }
        }

		//lo imprimimos ya ordenado
        printf("\nEl vector ordenado de forma descendente es:\n ");
        for (i = 0; i < tam; i++) {
            printf("%d  ", vec[i]);
        }
        
		//calculo de la media
        printf("\n\n ");
        float suma=0;
		for (i = 0; i < tam; i++) {
			suma=suma+vec[i];
        }
                printf("\n");

        printf("La media es: %f",suma/tam);
		printf("\nEl mayor valor de la matriz  es: %d y el menor de ella es: %d\n",vec[0],vec[tam-1]);
		return 0;
}
