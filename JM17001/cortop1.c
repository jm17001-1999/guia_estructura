#include <stdio.h>
#include <stdlib.h>

int main() {
    int tam, repuesto, divisores=0, contadorp=0, contadora=0, i, a, b;
    printf("Numero de filas y columnas\n");
    scanf("%d",&tam);
    

    int matriz1[tam][tam];
    int matriz2[tam][tam];
    int matriz3[tam][tam];
    
    //llenamos matriz1
    printf("Llenando Matriz 1:\n");
    for(i=0; i< tam; i++){
		for(a=0; a< tam; a++){
                    printf("Posicion ( %d , %d ): ",i,a);
                    scanf("%d",&matriz1[i][a]);	
		}	
    }
	
	//llenamos matriz2
	printf("Llenando Matriz 2:\n");
    for(i=0; i< tam; i++){
		for(a=0; a< tam; a++){
                    printf("Posicion ( %d , %d ): ",i,a);
                    scanf("%d",&matriz2[i][a]);	
		}	
    }
     
    //imprimimos las matrices   
	printf("\n Matriz 1:\n"); 
	        for(i=0;i<tam;i++){
	            for(a=0;a<tam;a++){
	                printf("%d ",matriz1[i][a]);
	                printf("\t");
				}
	            printf("\n");
	        }
	        
	        
	printf("\n Matriz 2:\n"); 
	        for(i=0;i<tam;i++){
	            for(a=0;a<tam;a++){
	                printf("%d ",matriz2[i][a]);
	                printf("\t");
				}
	            printf("\n");
	        }
    
		//hacemos la multiplicacion y la imprimimos
        printf("La matriz resultante es: \n");
	    for(int i=0;i<tam;i++){
            for(int a=0;a<tam;a++){
                matriz3[i][a]=0;
                for(b=0;b<tam;b++){
                    matriz3[i][a]=matriz3[i][a]+(matriz1[i][b]*matriz2[b][a]);
                    
				}
                    printf("%d ",matriz3[i][a]);
            }
                    printf("\n");
		}
        
        //busqueda de numeros primos
		printf("Numeros Primos: \n");  
       for(i=0;i<tam;i++){
	   for(a=0;a<tam;a++){
               for(b=1;b<=matriz3[i][a];b++){
                   if (matriz3[i][a]%b==0) {
                       divisores++;      
                    }                       
                }
                if (divisores==2) {
                   contadorp++;
                printf("%d posicion (%d,%d)  ",matriz3[i][a],i,a);
                }
               divisores=0;
            }
	}
	//si hay primos que siga.
	if (contadorp>0){
       
        printf("\n");
        int vector[contadorp];
        for(i=0;i<tam;i++){
			for(a=0;a<tam;a++){
               for(b=1;b<=matriz3[i][a];b++){
                   if (matriz3[i][a]%b==0) {
                       divisores++;      
                    }                       
                }
                if (divisores==2) {
                    vector[contadora]=matriz3[i][a];
                    contadora++;
                }
               divisores=0;
            }
	}


    for (i = 0; i <contadorp-1; i++) {
        for(a=0; a<contadorp-1; a++){
            if (vector[a]>vector[a+1]) {
                repuesto=vector[a];
                vector[a]=vector[a+1];
                vector[a+1]=repuesto;
            }
        }          
    }
		
		//imprimimos el vector de primos
        printf("\nPrimos:\n");
    for (i = 0; i < contadorp; i++) {
        printf("%d ",vector[i]); 

    }
} else {
	printf("\nNo hay numeros primos.\n");
}
  printf("\n"); 
        
    return 0;
}
